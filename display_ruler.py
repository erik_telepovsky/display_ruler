#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Display ruler.

Usage:
  display_ruler.py calculate by (--width=<cm>|--height=<cm>) [--ratio=<ratio>]
  display_ruler.py calculate by --diagonal=<float> [--unit=<unit>] [--ratio=<ratio>]
  display_ruler.py convert <size> to (inches|cm)
  display_ruler.py (-h | --help)
  display_ruler.py --version

Options:
  -h --help           Show this screen.
  --version           Show version.
  --width=<cm>        Width of display in cm
  --height=<cm>       Height of display in cm
  --diagonal=<float>  Size of diagonal
  --unit=<unit>       Unit (inches or cm) [default: cm]
  --ratio=<ratio>     Ratio of width and height (16:9 or 16:10) [default: 16:9]
  <size>              Size in cm or inches

"""
import math
from docopt import docopt


def to_inches(cm):
    return float(cm) / inch_in_cm


def to_cm(inches):
    return float(inches) * inch_in_cm


if __name__ == '__main__':
    arguments = docopt(__doc__, version='Display Ruler 1.0')
    ratios = ['16:9', '16:10']
    units = ['inches', 'cm']
    inch_in_cm = 2.54
    ratio_k = None
    width = None
    height = None
    diagonal_cm = None
    diagonal_inches = None
    # print(arguments)

    ratio = arguments.get('--ratio', None)
    if ratio:
        if ratio not in ratios:
            exit('Possible ratios are %s' % ratios)
        ratio_split = ratio.split(':')
        ratio_k = float(ratio_split[0])/int(ratio_split[1])

    unit = arguments.get('--unit', None)
    if unit:
        if unit not in units:
            exit('Possible units are %s' % units)
    
    if arguments.get('calculate', False):
        # calculate by (--width=<cm>|--height=<cm>) [--ratio=<ratio>]
        if arguments.get('--width', False) or arguments.get('--height', False):
            width = arguments.get('--width')
            height = arguments.get('--height')
            if width:
                width = float(width)
                height = width / ratio_k
            elif height:
                height = float(height)
                width = height * ratio_k

            diagonal_cm = float(math.sqrt(width * width + height * height))
            diagonal_inches = diagonal_cm / inch_in_cm
      
        # calculate by --diagonal=<float> [--unit=<unit>] [--ratio=<ratio>]
        elif arguments.get('--diagonal', False):
            diagonal = float(arguments.get('--diagonal'))
            if unit == 'cm':
                diagonal_cm = diagonal
                diagonal_inches = to_inches(diagonal_cm)
            else:
                diagonal_inches = diagonal
                diagonal_cm = to_cm(diagonal_inches)

            x = float(math.sqrt(diagonal_cm * diagonal_cm / (ratio_k * ratio_k + 1)))
            width = x * ratio_k
            height = x

        print 'Dimensions: %.2f x %.2f cm (%s)' % (width, height, ratio)
        print 'Diagonal: %.2f cm (%.2f inches)' % (diagonal_cm, diagonal_inches)

    # convert <size> to (inches|cm)
    elif arguments.get('convert', False):
        size = float(arguments.get('<size>'))
        if arguments.get('cm', False):
            print '%s inches is %.2f cm' % (size, to_cm(float(size)))
        else:
            print '%s cm is %.2f inches' % (size, to_inches(float(size)))
